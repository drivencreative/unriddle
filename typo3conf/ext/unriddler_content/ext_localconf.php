<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\FluidTYPO3\Flux\Core::registerProviderExtensionKey('unriddler_content', 'Page');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('unriddler_content', 'Content');
