<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Sudra.' . $_EXTKEY,
	'Unriddlequiz',
	array(
		'Feuser' => 'list, show',
		'Questions' => 'list, show',
		'Comments' => 'list, show, new, create, edit, update, delete',
		
	),
	// non-cacheable actions
	array(
		'Feuser' => '',
		'Questions' => '',
		'Comments' => 'create, update, delete',
		
	)
);
