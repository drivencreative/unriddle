<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Sudra.' . $_EXTKEY,
	'Unriddlequiz',
	'UnRiddle Quiz'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'UnRiddler');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_unriddler_domain_model_questions', 'EXT:unriddler/Resources/Private/Language/locallang_csh_tx_unriddler_domain_model_questions.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_unriddler_domain_model_questions');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_unriddler_domain_model_comments', 'EXT:unriddler/Resources/Private/Language/locallang_csh_tx_unriddler_domain_model_comments.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_unriddler_domain_model_comments');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_unriddler_domain_model_helps', 'EXT:unriddler/Resources/Private/Language/locallang_csh_tx_unriddler_domain_model_helps.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_unriddler_domain_model_helps');
