<?php
namespace Sudra\Unriddler\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Dragan Radisic <scorrp@gmail.com>, SudraLabs
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * CommentsController
 */
class CommentsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $comments = $this->commentsRepository->findAll();
        $this->view->assign('comments', $comments);
    }
    
    /**
     * action show
     *
     * @param \Dr\Unriddler\Domain\Model\Comments $comments
     * @return void
     */
    public function showAction(\Dr\Unriddler\Domain\Model\Comments $comments)
    {
        $this->view->assign('comments', $comments);
    }
    
    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        
    }
    
    /**
     * action create
     *
     * @param \Dr\Unriddler\Domain\Model\Comments $newComments
     * @return void
     */
    public function createAction(\Dr\Unriddler\Domain\Model\Comments $newComments)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->commentsRepository->add($newComments);
        $this->redirect('list');
    }
    
    /**
     * action edit
     *
     * @param \Dr\Unriddler\Domain\Model\Comments $comments
     * @ignorevalidation $comments
     * @return void
     */
    public function editAction(\Dr\Unriddler\Domain\Model\Comments $comments)
    {
        $this->view->assign('comments', $comments);
    }
    
    /**
     * action update
     *
     * @param \Dr\Unriddler\Domain\Model\Comments $comments
     * @return void
     */
    public function updateAction(\Dr\Unriddler\Domain\Model\Comments $comments)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->commentsRepository->update($comments);
        $this->redirect('list');
    }
    
    /**
     * action delete
     *
     * @param \Dr\Unriddler\Domain\Model\Comments $comments
     * @return void
     */
    public function deleteAction(\Dr\Unriddler\Domain\Model\Comments $comments)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        $this->commentsRepository->remove($comments);
        $this->redirect('list');
    }

}