<?php
namespace Sudra\Unriddler\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Dragan Radisic <scorrp@gmail.com>, SudraLabs
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Questions
 */
class Questions extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * question
     *
     * @var string
     */
    protected $question = '';
    
    /**
     * answer
     *
     * @var string
     */
    protected $answer = '';
    
    /**
     * help
     *
     * @var \Sudra\Unriddler\Domain\Model\Helps
     */
    protected $help = null;
    
    /**
     * comments
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sudra\Unriddler\Domain\Model\Comments>
     * @cascade remove
     */
    protected $comments = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->comments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the question
     *
     * @return string $question
     */
    public function getQuestion()
    {
        return $this->question;
    }
    
    /**
     * Sets the question
     *
     * @param string $question
     * @return void
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }
    
    /**
     * Returns the answer
     *
     * @return string $answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }
    
    /**
     * Sets the answer
     *
     * @param string $answer
     * @return void
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }
    
    /**
     * Returns the help
     *
     * @return \Sudra\Unriddler\Domain\Model\Helps help
     */
    public function getHelp()
    {
        return $this->help;
    }
    
    /**
     * Sets the help
     *
     * @param \Sudra\Unriddler\Domain\Model\Helps $help
     * @return void
     */
    public function setHelp(\Sudra\Unriddler\Domain\Model\Helps $help)
    {
        $this->help = $help;
    }
    
    /**
     * Adds a Comments
     *
     * @param \Sudra\Unriddler\Domain\Model\Comments $comment
     * @return void
     */
    public function addComment(\Sudra\Unriddler\Domain\Model\Comments $comment)
    {
        $this->comments->attach($comment);
    }
    
    /**
     * Removes a Comments
     *
     * @param \Sudra\Unriddler\Domain\Model\Comments $commentToRemove The Comments to be removed
     * @return void
     */
    public function removeComment(\Sudra\Unriddler\Domain\Model\Comments $commentToRemove)
    {
        $this->comments->detach($commentToRemove);
    }
    
    /**
     * Returns the comments
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sudra\Unriddler\Domain\Model\Comments> comments
     */
    public function getComments()
    {
        return $this->comments;
    }
    
    /**
     * Sets the comments
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sudra\Unriddler\Domain\Model\Comments> $comments
     * @return void
     */
    public function setComments(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $comments)
    {
        $this->comments = $comments;
    }

}