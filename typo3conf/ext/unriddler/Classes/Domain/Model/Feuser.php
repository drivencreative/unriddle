<?php
namespace Sudra\Unriddler\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Dragan Radisic <scorrp@gmail.com>, SudraLabs
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Feuser
 */
class Feuser extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * points
     *
     * @var float
     */
    protected $points = 0.0;
    
    /**
     * questions
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sudra\Unriddler\Domain\Model\Questions>
     */
    protected $questions = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->questions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the points
     *
     * @return float $points
     */
    public function getPoints()
    {
        return $this->points;
    }
    
    /**
     * Sets the points
     *
     * @param float $points
     * @return void
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }
    
    /**
     * Adds a Questions
     *
     * @param \Sudra\Unriddler\Domain\Model\Questions $question
     * @return void
     */
    public function addQuestion(\Sudra\Unriddler\Domain\Model\Questions $question)
    {
        $this->questions->attach($question);
    }
    
    /**
     * Removes a Questions
     *
     * @param \Sudra\Unriddler\Domain\Model\Questions $questionToRemove The Questions to be removed
     * @return void
     */
    public function removeQuestion(\Sudra\Unriddler\Domain\Model\Questions $questionToRemove)
    {
        $this->questions->detach($questionToRemove);
    }
    
    /**
     * Returns the questions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sudra\Unriddler\Domain\Model\Questions> questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }
    
    /**
     * Sets the questions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sudra\Unriddler\Domain\Model\Questions> $questions
     * @return void
     */
    public function setQuestions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $questions)
    {
        $this->questions = $questions;
    }

}