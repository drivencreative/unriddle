<?php
namespace Sudra\Unriddler\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Dragan Radisic <scorrp@gmail.com>, SudraCode
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Sudra\Unriddler\Controller\CommentsController.
 *
 * @author Dragan Radisic <scorrp@gmail.com>
 */
class CommentsControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \Sudra\Unriddler\Controller\CommentsController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('Sudra\\Unriddler\\Controller\\CommentsController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllCommentssFromRepositoryAndAssignsThemToView()
	{

		$allCommentss = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$commentsRepository = $this->getMock('', array('findAll'), array(), '', FALSE);
		$commentsRepository->expects($this->once())->method('findAll')->will($this->returnValue($allCommentss));
		$this->inject($this->subject, 'commentsRepository', $commentsRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('commentss', $allCommentss);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenCommentsToView()
	{
		$comments = new \Sudra\Unriddler\Domain\Model\Comments();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('comments', $comments);

		$this->subject->showAction($comments);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenCommentsToCommentsRepository()
	{
		$comments = new \Sudra\Unriddler\Domain\Model\Comments();

		$commentsRepository = $this->getMock('', array('add'), array(), '', FALSE);
		$commentsRepository->expects($this->once())->method('add')->with($comments);
		$this->inject($this->subject, 'commentsRepository', $commentsRepository);

		$this->subject->createAction($comments);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenCommentsToView()
	{
		$comments = new \Sudra\Unriddler\Domain\Model\Comments();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('comments', $comments);

		$this->subject->editAction($comments);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenCommentsInCommentsRepository()
	{
		$comments = new \Sudra\Unriddler\Domain\Model\Comments();

		$commentsRepository = $this->getMock('', array('update'), array(), '', FALSE);
		$commentsRepository->expects($this->once())->method('update')->with($comments);
		$this->inject($this->subject, 'commentsRepository', $commentsRepository);

		$this->subject->updateAction($comments);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenCommentsFromCommentsRepository()
	{
		$comments = new \Sudra\Unriddler\Domain\Model\Comments();

		$commentsRepository = $this->getMock('', array('remove'), array(), '', FALSE);
		$commentsRepository->expects($this->once())->method('remove')->with($comments);
		$this->inject($this->subject, 'commentsRepository', $commentsRepository);

		$this->subject->deleteAction($comments);
	}
}
